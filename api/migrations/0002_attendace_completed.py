# Generated by Django 4.0 on 2021-12-27 07:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='attendace',
            name='completed',
            field=models.BooleanField(default=False),
        ),
    ]

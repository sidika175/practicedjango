from django.db import models
from django.db.models.deletion import CASCADE, DO_NOTHING
from django.db.models.expressions import Case, CombinedExpression
from django.db.models.fields import BooleanField, IntegerField, TextField, TimeField
from django.db.models.fields.json import CaseInsensitiveMixin
from django.db.models.fields.related import ForeignKey

class Students(models.Model):
    id      = models.AutoField(primary_key=True)
    nim     = TextField(max_length=12)
    name    = TextField(max_length=50)
    gender  = TextField(max_length=1)
    address = TextField(max_length=100)

class Lecturers(models.Model):
    id      = models.AutoField(primary_key=True)
    nikL     = TextField(max_length=16)
    name    = TextField(max_length=50)
    gender  = TextField(max_length=1)
    address = TextField(max_length=100)

class Courses(models.Model):
    id          = models.AutoField(primary_key=True)
    code        = TextField(max_length=5)
    name        = TextField(max_length=30)
    sks         = IntegerField()
    desc        = TextField(max_length=100)

class Schedule(models.Model):
    id          = models.AutoField(primary_key=True)
    code        = TextField()
    time        = TimeField()
    nikLecturer = ForeignKey('Lecturers', related_name='nik', on_delete=DO_NOTHING)
    courseCode  = ForeignKey('Courses', related_name='courseCode', on_delete=DO_NOTHING)

class UnivLocation(models.Model):
    id          = models.AutoField(primary_key=True)
    univId      = TextField()
    name        = TextField()
    latitude    = TextField()
    longitude   = TextField()

# class Attendace(models.Model):
#     id          = models.AutoField(primary_key=True)
#     classMate   = TextField(max_length=5)
#     nik         = TextField(max_length=18)
#     nim         = TextField(max_length=12)
#     latitude    = TextField()
#     laongitude  = TextField()
#     completed   = BooleanField(default=False)

#     owner       = models.ForeignKey('auth.User', related_name='attId', on_delete=models.CASCADE)
#     highlighted = models.TextField()
    
from rest_framework import serializers
from django.db.models import fields
from .models import Students, Courses, Lecturers, Schedule, UnivLocation
from django.contrib.auth.models import User

class StudentsSerializer(serializers.ModelSerializer):

    # owner = serializers.ReadOnlyField(source='owner.username')

    class Meta:
        model   = Students
        fields  = ('nim', 'name', 'gender', 'address')

class LecturersSerializer(serializers.ModelSerializer):

    class Meta:
        model   = Lecturers
        fields  = ('nikL', 'name', 'gender', 'address')

class CoursesSerializer(serializers.ModelSerializer):

    class Meta:
        model   = Courses
        fields  = ('code', 'name', 'sks', 'desc')

class ShceduleSerializer(serializers.ModelSerializer):

    class Meta:
        model   = Schedule
        fields  = ('code', 'time', 'nikLecturer', 'courseCode')

class UnivLocationSerializer(serializers.ModelSerializer):

    class Meta:
        model   = UnivLocation
        fields  = ('univId', 'name', 'latitude', 'longtitude')



# class UserSerializer(serializers.ModelSerializer):

#     attId = serializers.PrimaryKeyRelatedField(many=True, queryset=Attendace.objects.all())

#     class Meta:
#         model   = User
#         fields  = ('id','username','attId')
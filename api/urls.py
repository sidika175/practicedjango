from django.urls import path, include
from rest_framework.urlpatterns import format_suffix_patterns
from django.urls import path, include
from rest_framework.routers import DefaultRouter
from api import views

router = DefaultRouter()
router.register(r'students', views.StudentViewSet, basename='Students')
router.register(r'lecturers', views.LecturerViewSet, basename='Lecturers')
router.register(r'courses', views.CourseViewSet, basename='Courses')
router.register(r'schedule', views.ScheduleViewSet, basename='Schedule')
router.register(r'univlocation', views.UnivLocationViewSet, basename='UnivLocation')

urlpatterns = [
    path('api/', include(router.urls)),
]

# urlpatterns = [
#     path('api/attendance/', views.AttendanceList.as_view()),
#     path('api/attendance/<int:pk>', views.AttendanceDetail.as_view()),
#     path('api/user/', views.UserList.as_view()),
#     path('api/user/<int:pk>', views.UserDetail.as_view()),
#     path('api-auth/login/', include('rest_framework.urls')),
# ]

# urlpatterns = format_suffix_patterns(urlpatterns)



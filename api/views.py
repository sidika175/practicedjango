from django.db.models.query import QuerySet
from .models import Schedule, Students, Lecturers, Courses, UnivLocation
from .serializers import ShceduleSerializer, StudentsSerializer, LecturersSerializer, CoursesSerializer, UnivLocationSerializer
from rest_framework import status, viewsets
from api import serializers
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.renderers import JSONRenderer
from rest_framework import permissions

class StudentViewSet(viewsets.ModelViewSet):
    
    QuerySet   = Students.objects.all()
    serializer_class = StudentsSerializer

    def get_queryset(self):
        students = Students.objects.all()
        return students

class LecturerViewSet(viewsets.ModelViewSet):
    
    QuerySet   = Lecturers.objects.all()
    serializer_class = LecturersSerializer

    def get_queryset(self):
        lecturers = Lecturers.objects.all()
        return lecturers

class CourseViewSet(viewsets.ModelViewSet):
    
    QuerySet   = Courses.objects.all()
    serializer_class = CoursesSerializer

    def get_queryset(self):
        courses = Courses.objects.all()
        return courses

class ScheduleViewSet(viewsets.ModelViewSet):
    
    QuerySet   = Schedule.objects.all()
    serializer_class = ShceduleSerializer

    def get_queryset(self):
        schedule = Schedule.objects.all()
        return schedule


class UnivLocationViewSet(viewsets.ModelViewSet):
    
    QuerySet   = UnivLocation.objects.all()
    serializer_class = UnivLocationSerializer

    def get_queryset(self):
        univlocation = UnivLocation.objects.all()
        return univlocation

    # def create(self, request, *args, **kwargs):
    #     serializer = self.get_serializer(data=request.data)
    #     if not serializer.is_valid(raise_exception=False):
    #         return Response({"Fail": "Failed"}, status=status.HTTP_400_BAD_REQUEST)

    #     self.perform_create(serializer)
    #     headers = self.get_success_headers(serializer.data)
    #     return Response({"Success": "Successed"}, status=status.HTTP_201_CREATED, headers=headers)
    

# class UserList(generics.ListAPIView):

#     permission_classes = [permissions.IsAuthenticatedOrReadOnly]

#     queryset = User.objects.all()
#     serializer_class = UserSerializer

# class UserDetail(generics.RetrieveAPIView):

#     permission_classes = [permissions.IsAuthenticatedOrReadOnly,IsOwnerOrReadOnly]

#     queryset = User.objects.all()
#     serializer_class = UserSerializer

# class AttendanceList(APIView):

#     permission_classes = [permissions.IsAuthenticatedOrReadOnly]

#     queryset = Attendace.objects.all()
#     serializer_class = AttendanceSerializer

#     def get(self, request, format=None):
#         attendance = Attendace.objects.all()
#         serializer = AttendanceSerializer(attendance, many=True)
#         return Response(serializer.data)
    
#     def post(self, request, format=None):
#         serializer = AttendanceSerializer(data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data, status=status.HTTP_201_CREATED)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

# class AttendanceDetail(APIView):

#     permission_classes = [permissions.IsAuthenticatedOrReadOnly,IsOwnerOrReadOnly]

#     queryset = Attendace.objects.all()
#     serializer_class = AttendanceSerializer


#     def perform_create(self, serializer):
#         serializer.save(owner=self.request.user)

#     def get_object(self, pk):
#         try:
#             return Attendace.objects.get(pk=pk)
#         except Attendace.DoesNotExist:
#             raise Http404

#     def get(self, request, pk, format=None):
#         attendance = self.get_object(pk)
#         serializer = AttendanceSerializer(attendance)
#         return Response(serializer.data)
    
#     def put(self, request, pk, format=None):
#         attendance = self.get_object(pk)
#         serializer = AttendanceSerializer(attendance, data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    # def delete(self, request, pk, format=None):
    #     attendance = self.get_object(pk)
    #     attendance.delete()
    #     return Response(status=status.HTTP_204_NO_CONTENT)
